#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''

habitual = ("patatas", "leche", "pan")


def main():
    var = True
    especifica = []
    rango = 0
    while var:
        compra = input("Elemento a comprar: ")
        rango += 1
        if compra != "":
            if compra not in especifica:
                especifica.append(compra)

        else:
            rango -= 1
            lista_compra = []
            for prod in habitual:
                lista_compra.append(prod)
            for product in especifica:
                if product not in lista_compra:
                    lista_compra.append(product)

            lista = set(lista_compra)
            print("Lista de la compra:")

            for producto in lista_compra:
                print(producto)

            print(f"Elementos habituales: {len(habitual)}")
            print(f"Elementos específicos: {rango}")
            print(f"Elementos en lista: {len(lista)}")
            var = False


if __name__ == '__main__':
    main()
